#!/usr/bin/ruby

# problem 1
def three_and_five
  total = 0
  for i in 0...1000
    total += i if i.modulo(3).zero? || i.modulo(5).zero?
  end
  puts "project euler 1 = #{total}"
end

# problem 2
def even_fib
  total = 0
  first = 0
  second = 1
  while second < 4000000
    total += second if second % 2 == 0
    second, first = first + second, second
  end
  puts "project euler 2 = #{total}"
end

three_and_five()
even_fib()
