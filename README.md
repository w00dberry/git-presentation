# Some Stuff About [Git](http://git-scm.com/)

## 1. Useful Things to Read

* [Git: The Safety Net for Your Projects](http://alistapart.com/article/git-the-safety-net-for-your-projects)
* [Git Tutorial](https://www.atlassian.com/git/tutorials) by Atlassian
* [Subversion Re-education](http://hginit.com/00.html) is a _very_ good article! It is technically about [Mercurial](http://mercurial.selenic.com) (which is very similar to Git) but the principles are the same.
* [Why You Should Switch from Subversion to Git](http://blog.teamtreehouse.com/why-you-should-switch-from-subversion-to-git) from the folks over at [Treehouse](http://teamtreehouse.com).
* [Pro Git](https://github.com/progit) book by [Scott Chacon](http://scottchacon.com) as well as his [GitHub Flow](http://scottchacon.com/2011/08/31/github-flow.html) blog post.
* [8 Reasons for Swtiching to Git](http://www.git-tower.com/blog/8-reasons-for-switching-to-git/) is greate. There is a particularly useful part about "Make Useful Commits"

## 2. Integration with IDEs

### Visual Studio
[According to the MSDN website](http://msdn.microsoft.com/en-us/library/hh850437.aspx), you can begin using Git decentralized version control before you write your first line of code with virtually no cost or risk. You get many benefits, including being able to revert to a known good state whenever you get in trouble. When you need to switch contexts, you can quickly create a private local branch. Later, you can either publish the branch or dispose of it. All this is done on your local dev machine, with no server required. When you’re ready, you can quickly share your code and begin collaborating in TFS or on a third-party Git service. You can use Visual Studio and Git to collaborate with your team using Team Foundation Server (on-premises or in the cloud), on CodePlex, or on a third-party service such as GitHub or Bitbucket.

### Eclipse
[Eclipse](https://www.eclipse.org/) has a plugin called [EGit](http://www.eclipse.org/egit/) that allows you to manage Git repositories directly from within the IDE itself. There are numerous other plugins but EGit seems to be the most popular.

### Netbeans
The latest version of [NetBeans](https://netbeans.org/) provides native support for Git. There is a [in depth tutorial](https://netbeans.org/kb/docs/ide/git.html) on the NetBeans website that provides step-by-step instructions for common taks like initializing a Git repository, cloning a Git respository, adding files to a Git repository, editing files, commiting sources to a respository, working with branches, and working with remote repositories.

### Sublime Text
[Sublime Text](http://www.sublimetext.com/) is a sophisticated text editor for code, markup and prose. It is very extensible and there are a lot of plugins available for it. There are certinaly plugins that allow you to perform Git operations on files but I use only a simple plugin called [GitGutter](https://github.com/jisaacks/GitGutter) that shows which lines have changed in relation to previous versions. It places icons in next to the line numbers, in the "gutter" to indicate changes to the currently open file. There is a plugin for anything you can think of in Sublime Text.

## 3. Account Management

### Bitbucket
Bitbucket offer an interface that allow an administrator to add and manage [Bitbucket Teams](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+Teams) through custom groups that can have specific permission levels (read only, write, admin access, no access, repository creation rights, etc) so you can quickly manage permissions for different roles. Furthermore, each repository can be fine tuned to include all groups, a set of groups, and/or individual users, and even override individual group/user permissions for that specific repository. Each repository can also setup custom permissions levels for [branching](http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging) and [rebasing](http://git-scm.com/book/ch3-6.html).

### From the Git Documentation
"If you’re a small outfit or are just trying out Git in your organization and have only a few developers, things can be simple for you. One of the most complicated aspects of setting up a Git server is user management. If you want some repositories to be read-only to certain users and read/write to others, access and permissions can be a bit difficult to arrange.

If you already have a server to which all your developers have SSH access, it’s generally easiest to set up your first repository there, because you have to do almost no work (as we covered in the last section). If you want more complex access control type permissions on your repositories, you can handle them with the normal filesystem permissions of the operating system your server runs.

If you want to place your repositories on a server that doesn’t have accounts for everyone on your team whom you want to have write access, then you must set up SSH access for them. We assume that if you have a server with which to do this, you already have an SSH server installed, and that’s how you’re accessing the server.

There are a few ways you can give access to everyone on your team. The first is to set up accounts for everybody, which is straightforward but can be cumbersome. You may not want to run adduser and set temporary passwords for every user.

A second method is to create a single 'git' user on the machine, ask every user who is to have write access to send you an SSH public key, and add that key to the ~/.ssh/authorized_keys file of your new 'git' user. At that point, everyone will be able to access that machine via the 'git' user. This doesn’t affect the commit data in any way — the SSH user you connect as doesn’t affect the commits you’ve recorded.

Another way to do it is to have your SSH server authenticate from an LDAP server or some other centralized authentication source that you may already have set up. As long as each user can get shell access on the machine, any SSH authentication mechanism you can think of should work."

## 4. Migration from or to SVN

Atlassian provides a [really great article/tutorial](https://www.atlassian.com/git/migration) on how to do this. I've personally completed the process of migrating our 1270-CCC_Facelift subversion repository into a Git repository, which is Riverside team members have access to [here](https://bitbucket.org/riversidetechnology/colorado-climate-center-svn-clone).

## 5. Other tools (workflow, code review, bug tracking, etc)

### TortoiseGit
[TortoiseGit](https://code.google.com/p/tortoisegit/) is a port of TortoiseSVN for Git. It could be nice for people used to using source control features from the windows explorer context menu. According to their documentation, TortoiseGit is for computer literate folk who want to use Git to manage their data, but are uncomfortable using the command line client to do so. Since TortoiseGit is a windows shell extension it's assumed that the user is familiar with the windows explorer and knows how to use it.

### Atlassian Sourcetree
This is a GUI for Git. My preference is to use this GUI in combination with the gitgutter plugin for Sublime Text. It provides a great visualization of the changes to the repository and assists with branching, merging, pushing and pulling, etc. It does inline code "chunk" editing and conflict resolution. It syncs up to GitHub and Bitbucket which is really convenient.

## Hosting a Git server on premise
I think the most important thing to note here is that setting up a cloud hosted Git service like Bitbucket provides us with a feature rich web interface that we can work with. This gives us a lot of things from the start:

*   User Account and User Group Management
*   Permissions
*   Repository Indexing/Searching/Sorting (that can be _bookmarked in a browser_
*   [Pull Requests](https://www.atlassian.com/git/workflows#!pull-request)
*   Specific Tutorials and Documentation
*   Issue Tracking

Compare that to a basic local Git server (check out the setup docs [here](http://git-scm.com/book/en/Git-on-the-Server)) in which we would have to implement all these features ourselves. Using a service like Bitbucket gives us a great way to interact with other user groups outside of the software development team, because they don't need to learn Git over the command line in order to use it. They can just go to a website.

### Atlassian Bitbucket/Stash

My personal preference in Git hosting would be [Bitbucket](https://bitbucket.org/) cloud hosting. One of the main advantages is that the repository library is accessible and searchable from a web interface. The web interface also includes user, team, and repository management sections.  It has basic issue tracking, commit history, markdown/html wiki, source file list, and a Readme home page for each repository.  You can subscribe to a repository's RSS feed and get live updates. [Bitbucket's pricing](https://bitbucket.org/account/user/w00dberry/plans/) is based on the number of users. It is basically $1 per user.

Bitbucket has some really nice tutorials online that cover [Git basics](https://www.atlassian.com/git/tutorial/git-basics) as well as more advanced topics like undoing changes, git branches, rewriting git history, and remote repositories. They also have some great tutorials about Git [workflows](https://www.atlassian.com/git/workflows) and they have a full featured [support](https://bitbucket.org/support) page and [Documentation](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+Documentation+Home).

One of the really great features I'd like to take advantage of is pull requests. Users can make changes in a branch and create a pull request to get the changes reviewed. You can the discuss and approve code changes, and then merge the branch.

[Atlassian](https://www.atlassian.com/) also offers a full range of other software development tools, including [Stash](https://www.atlassian.com/software/stash), which is an on premise version of their cloud hosting service (Bitbucket). A Stash license is only $10 dollars for ten people, but it goes up from there quite a bit. You can see the [full pricing](https://www.atlassian.com/software/stash/pricing) online. A 25 user license is $1800, a 50 user license is $3,300 and so on. These licenses entitle the owner to perpetual use, and come with 12 months of product updates and support.

Atlassian also offers some other tools that I think would be worth trying, specifically [JIRA](https://www.atlassian.com/software/jira) which is a project management tool that costs $10 for ten users (so we could use it for the software team).